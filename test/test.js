const { assert } = require('chai');
const newUser = {
  email: 'juan.delacruz@gmail.com',
  password: 'thequickbrownfoxjumpsoverthelazydog',
};

it('newUser_is_of_type_object', () => {
  assert.typeOf(newUser, 'object');
});

it('email_is_of_type_string', () => {
  assert.typeOf(newUser.email, 'string');
});

it('email_is_not_undefined', () => {
  assert.isDefined(newUser.email);
});

it('password_is_of_type_string', () => {
  assert.typeOf(newUser.password, 'string');
});

it('password_is_not_undefined', () => {
  assert.isDefined(newUser.password);
});

it('password_length_is_at_least_16', () => {
  assert.isAtLeast(newUser.password.length, 16);
});
